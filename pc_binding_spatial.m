function pc_binding_spatial()

%define network weights
locations=2;
features=4;
W=zeros(features,locations*features);
k=0;
for j=1:features
  W(j,[j:features:locations*features])=1;
end
W=norm_weights(W);
[n,m]=size(W);

%define inputs to network
X=zeros(m,2);
X([1,3,6,8],1:2)=1;
xscale=0.65;

a(:,:,1)=zeros(m,size(X,2));	 
a(:,:,2)=zeros(m,size(X,2));
a(1:4,2,1)=1;
recorded_time=20;

%simulate experiment
figure(1); clf
figure(2); clf
for p=1:size(X,2)
 	x=X(:,p).*xscale;
	
	[Y2,Y1]=linear_PCBC_attention_model(x,W,a(:,p,:),0.2,1,0,100);
	y2=Y2(:,recorded_time);
	y1=Y1(:,recorded_time);
	figure(1); subplot(9,5,[p:5:5*5]);
	plot_network(y1,W',y2,0,0,a(:,p,1),a(:,p,2),1);
	plot_network_enhance(x,m,n);

	[Y2,Y1]=nonlinear_PCBC_attention_model(x,W,a(:,p,:),0.3,100);
	y2=Y2(:,recorded_time);
	y1=Y1(:,recorded_time);
	figure(2); subplot(9,5,[p:5:5*5]);
	plot_network(y1,W',y2,0,0,a(:,p,1),a(:,p,2),1);
	plot_network_enhance(x,m,n);
end


