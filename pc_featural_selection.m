function pc_featural_selection(alg)
%alg      1=linear_PC/BC, 2=nonlinear_PC/BC

if nargin<1, alg=1; end

if alg==1 
  w11=0.8; w22=0.3; %define weights to node 1 and node 2
  eta=0.1;zeta=1;vartheta=0; %define parameters for algorithm
elseif alg==2
  w11=0.8; w22=0.5; %define weights to node 1 and node 2
  eta=0.1; %define parameters for algorithm
end
%create weight matrix
W=[w11,1-w11;
   1-w22,w22];
%define bottom input for each trial
xscale=0.85;
X=[1,0,1,1;
   0,1,1,1].*xscale;
stim_off=13; %define iteration at which stimulus is removed from input
%define attentional state for each trial
attn(:,:,2)=[1,0,1,0;
			 0,1,0,1];
attn(:,:,1)=zeros(size(attn(:,:,2)));

%simulate experiment
resp=[];
for p=1:size(X,2)
  x=X(:,p);
  a=attn(:,p,:);
  if alg==1
	y=linear_PCBC_attention_model(x,W,a,eta,zeta,vartheta,stim_off);
  elseif alg==2
	y=nonlinear_PCBC_attention_model(x,W,a,eta,stim_off);
  end
  
  resp=[resp;y(1,:)];%collect response traces for node 1 over different
					   %test conditions
end

%plot the results
clf
subplot(1,3,[1:2])
hands=plot(resp');
rr=[0.85,0,0];gg=[0,0.85,0];bb=[0,0,0.85];
style=['--';'-.';'- ';'- '];
color=[rr;gg;rr;gg];
width=[6;4;6;4];
for j=1:size(X,2)
  set(hands(j),'LineStyle',style(j,:),'Color',color(j,:),'LineWidth',width(j));
end
plotxmin=0;
plotxmax=length(resp);
plotymax=1.05*max(max(resp));
plotymin=min(min(resp))-0.025;
axis([plotxmin,plotxmax,plotymin,plotymax])	
hold on
plot([plotxmin,plotxmax],[0,0],'k-')
set(gca,'FontSize',18);
xlabel('Time'),
ylabel('Response');
legend('pref attend pref','poor attend poor','pair attend pref','pair attend poor','Location',[0.75,0.75,0.01,0.01]);













