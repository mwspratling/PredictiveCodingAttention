function pc_spatial_selection_contrast(alg,sel)
%alg      1=linear_PC/BC, 2=nonlinear_PC/BC
%sel      1=high selectivity, 2=low selectivity

if nargin<1, alg=1; end

if alg==1 
  if sel==1
	w11=0.9; w22=0.6; %define weights to node 1 and node 2
  else
	w11=0.7; w22=0.6; %define weights to node 1 and node 2
  end
  eta=0.2;zeta=1;vartheta=0; %define parameters for algorithm
elseif alg==2
  if sel==1
	w11=0.9; w22=0.7; %define weights to node 1 and node 2
  else
	w11=0.7; w22=0.7; %define weights to node 1 and node 2
  end
  eta=0.5; %define parameters for algorithm
end
%create weight matrix
W=[w11,1-w11;
   1-w22,w22];
%define bottom input for each trial
xscale=0.4;
X=[1,0,1,1;
   0,1,1,1].*xscale;
contrasts=[0.125,0.25,0.5,1,2];
stim_off=13; %define iteration at which stimulus is removed from input
%define attentional state for each trial
attn(:,:,1)=[0,0,0,0;
			 0,0,0,1];
attn(:,:,2)=zeros(size(attn(:,:,1)));

clf
%simulate experiment
for c=1:length(contrasts)
  resp=[];
  for p=1:size(X,2)
	x=X(:,p);
	x(2,:)=x(2,:).*contrasts(c); 
	a=attn(:,p,:);
	if alg==1
	  y=linear_PCBC_attention_model(x,W,a,eta,zeta,vartheta,stim_off);
	elseif alg==2
	  y=nonlinear_PCBC_attention_model(x,W,a,eta,stim_off);
	end
  
	resp=[resp;y(1,:)];%collect response traces for node 1 over different
					   %test conditions
  end
  %calculae AMI
  rtmp=resp(:,4:13);
  rtmp(find(rtmp<0))=0;
  rave=sum(rtmp');
  AMI=(rave(4)-rave(3))./(rave(4)+rave(3))

  %plot the results
  subplot(2,5,c+5)
  hands=plot(resp');
  rr=[0.85,0,0];gg=[0,0.85,0];bb=[0,0,0.85];
  style=['--';'-.';'- ';'- '];
  color=[bb;bb;bb;gg];
  width=[2;2;2;4];
  for j=1:size(X,2)
	set(hands(j),'LineStyle',style(j,:),'Color',color(j,:),'LineWidth',width(j));
  end
  plotxmin=-1;
  plotxmax=length(resp)-3;
  plotymax=0.55;
  if alg==1, plotymin=-0.4;
  else plotymin=-0.025; end
  axis([plotxmin,plotxmax,plotymin,plotymax])	
  hold on
  plot([plotxmin,plotxmax],[0,0],'k-')
  set(gca,'FontSize',18);
  if c==3, xlabel('Time'), end
  if c==1, ylabel('Response'); else
  set(gca,'YTickLabel',[]); end
end
legend('pref attend away','poor attend away','pair attend away','pair attend poor','Location',[0.75,0.75,0.01,0.01]);












