function pc_binding()

%define network weights
locations=2;
features=2;
W=zeros(locations*features,locations+features);
k=0;
for i=1:locations
  for j=1:features
	k=k+1;
	W(k,i)=1;
	W(k,j+locations)=1;
  end
end
W=norm_weights(W);
[n,m]=size(W);

%define inputs to network
X=zeros(m,5);
X(1:4,1:2)=1;
X(1,4:5)=1;
X(4,4:5)=1;
X(2:m,6)=1;
X(1:4,3)=[1,0.769,1,0.769]';
xscale=0.65;

a(:,:,1)=zeros(m,size(X,2));	 
a(:,:,2)=zeros(n,size(X,2));
a(1,2,2)=1;
a(1,5,2)=1;
recorded_time=20;

%simulate experiment
figure(1); clf
figure(2); clf
for p=1:size(X,2)
 	x=X(:,p).*xscale;

	[Y2,Y1]=linear_PCBC_attention_model(x,W,a(:,p,:),0.2,1,0,100);
	y2=Y2(:,recorded_time);
	y1=Y1(:,recorded_time);
	figure(1); subplot(3,size(X,2),p);
	plot_network(y1,W',y2,0,0,a(:,p,1),a(:,p,2),1);
	plot_network_enhance(x,m,n);

	[Y2,Y1]=nonlinear_PCBC_attention_model(x,W,a(:,p,:),0.3,100);
	y2=Y2(:,recorded_time);
	y1=Y1(:,recorded_time);
	figure(2); subplot(3,size(X,2),p);
	plot_network(y1,W',y2,0,0,a(:,p,1),a(:,p,2),1);
	plot_network_enhance(x,m,n);
end



