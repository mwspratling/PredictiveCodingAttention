function plot_network_enhance(x,m,n)
if m==4 
  text(zeros(1,m)-0.75,-[1:m],[' B';' R';' 0';'90'],...
	   'HorizontalAlignment','right','VerticalAlignment','middle','FontSize',6)
  text(ones(1,n)+0.3,-[1:n],['B-0 ';'B-90';'R-0 ';'R-90'],...
	   'HorizontalAlignment','left','VerticalAlignment','middle','FontSize',6)
  equalarrowaspect=2/3;
end
if m==8
  text(zeros(1,m)-0.55,-[1:m],[' B';' R';' 0';'90';' B';' R';' 0';'90'],...
	   'HorizontalAlignment','right','VerticalAlignment','middle','FontSize',6)
  text(ones(1,n)+0.3,-[1:n]-2,['B ';'R ';'0 ';'90'],...
	   'HorizontalAlignment','left','VerticalAlignment','middle','FontSize',6)
  equalarrowaspect=0.5;
end
axis([-0.8,1.8,-max([m,n])-0.5,-0.25])
ia=0-sqrt(2)*0.25*equalarrowaspect;
for j=1:m
  if x(j)>0.01
	ja=-j;
	l=0.3/sqrt(2);
	plot([ia+0.05/sqrt(2),ia-sqrt(2)*0.5*equalarrowaspect],[ja,ja],'r-','LineWidth',2);
	plot([ia,ia-l*equalarrowaspect],[ja,ja-l],'r-','LineWidth',2);
	plot([ia,ia-l*equalarrowaspect],[ja,ja+l],'r-','LineWidth',2);
  end
end


