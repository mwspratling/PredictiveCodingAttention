function [W,What]=norm_weights(W)
[n,m]=size(W);
%normalise weights
for j=1:n
  W(j,:)=W(j,:)./norm(W(j,:),1);
  What(j,:)=W(j,:)./norm(W(j,:),inf);
end

