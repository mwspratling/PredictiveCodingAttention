function pc_featural_tuning_curve()

%LINEAR PC/BC

%define network weights
m=13;
for j=1:m
  dist=min([abs(j-[1:m]);abs(j+(m-[1:m]));abs(j-(m+[1:m]))]);
  W(j,:)= exp(dist.^2./(-2*(0.2*m).^2));  
end
W=norm_weights(W);
[n,m]=size(W);
recorded_node=ceil(n/2);
recorded_time=20;

%define inputs to network
for i=1:m
  dist=min([abs(i-[1:m]);abs(i+(m-[1:m]));abs(i-(m+[1:m]))]);
  X(:,i)= exp(dist.^2./(-2*(0.15*m).^2))';
end
xscale=0.65;
stim_off=100; %define iteration at which stimulus is removed from input
attn_away=zeros(n,1,2);

Yn=[];
Ya=[];
%simulate experiment
for p=1:size(X,2)
  x=X(:,p).*xscale;

  %measure response without attention
  Y=linear_PCBC_attention_model(x,W,attn_away,0.2,1,0,stim_off);
  Yn=[Yn,Y(recorded_node,recorded_time)];

  %measure response with attention
  attn_here(:,1,2)=x;
  Y=linear_PCBC_attention_model(x,W,attn_here,0.2,1,0,stim_off);
  Ya=[Ya,Y(recorded_node,recorded_time)];
end

figure(1); clf
plot_nice(Yn,'b-s')
plot_nice(Ya,'r-o');
plot([1,m],[0,0],'k')
axis([1,m,-0.2,max([Yn,Ya])*1.1])
set(gca,'XTick',[],'YTick',[],'FontSize',36);
ylabel('Response')
xlabel('Direction')



%NONLINEAR PC/BC

%define network weights
for j=1:m
  dist=min([abs(j-[1:m]);abs(j+(m-[1:m]));abs(j-(m+[1:m]))]);
  W(j,:)= exp(dist.^2./(-2*(0.4*m).^2));  
end
W=norm_weights(W);
[n,m]=size(W);

%define inputs to network
for i=1:m
  dist=min([abs(i-[1:m]);abs(i+(m-[1:m]));abs(i-(m+[1:m]))]);
  X(:,i)= exp(dist.^2./(-2*(0.4*m).^2))';
end
attn_away=zeros(n,1,2);

Yn=[];
Ya=[];
%simulate experiment
for p=1:size(X,2)
  x=X(:,p).*xscale;

  %measure response without attention
  Y=nonlinear_PCBC_attention_model(x,W,attn_away,0.3,stim_off);
  Yn=[Yn,Y(recorded_node,recorded_time)];
  
  %measure response with attention
  attn_here(:,1,2)=x;
  Y=nonlinear_PCBC_attention_model(x,W,attn_here,0.3,stim_off);
  Ya=[Ya,Y(recorded_node,recorded_time)];
end


figure(2); clf
plot_nice(Yn,'b-s')
plot_nice(Ya,'r-o');
plot([1,m],[0,0],'k')
axis([1,m,-0.01,max([Yn,Ya])*1.1])
set(gca,'XTick',[],'YTick',[],'FontSize',36);
ylabel('Response')
xlabel('Direction')



function plot_nice(y,colour)
plot(y,colour,'LineWidth',8,'MarkerFaceColor','w','MarkerSize',20)
hold on




