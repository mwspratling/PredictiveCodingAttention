function pc_posner()
xscale=0.65;

W=[1,1];
W=norm_weights(W);
[n,m]=size(W);
x=[1;0].*xscale;

a=[0.8,0.5,0.2];
attn(1,:,1)=a;
attn(2,:,1)=fliplr(a);
attn(:,:,2)=zeros(2,length(a));


Yl=[];
Yn=[];
for p=1:length(a)
  y2=linear_PCBC_attention_model(x,W,attn(:,p,:),0.2,1,0,100);
  Yl=[Yl;y2];

  y2=nonlinear_PCBC_attention_model(x,W,attn(:,p,:),0.3,100);
  Yn=[Yn;y2];
end
figure(1); clf
yl=1-Yl(:,size(Yl,2));%reation time would be shorter for higher activation
plot(a,yl,'b-o','LineWidth',8,'MarkerFaceColor','w','MarkerSize',25)
set(gca,'FontSize',32,'XTick',fliplr(a),'YTick',[]);
axis([0,1,0.15,0.66])
ylabel('1-Response')
xlabel('Cue Validity')

figure(2); clf
yn=1-Yn(:,size(Yn,2)); %reation time would be shorter for higher activation
plot(a,yn,'b-o','LineWidth',8,'MarkerFaceColor','w','MarkerSize',25)
set(gca,'FontSize',32,'XTick',fliplr(a),'YTick',[]);
axis([0,1,0.55,0.66])
ylabel('1-Response')
xlabel('Cue Validity')

