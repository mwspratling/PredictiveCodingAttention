function [y,z,e]=nonlinear_PCBC_iteration(x,y,W,What,z,a,eta);
%update the response of a processing stage in the nonlinear PC/BC model. This
%uses the Divisive Input Modulation (DIM) of competition.
epsilon=1e-10;

e=x./(epsilon+(What'*y));
y=(epsilon+y).*(W*e);
y=y.*(1+eta.*(a+z));

z=W'*y; %feedback to lower processing stage

