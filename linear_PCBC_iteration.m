function [y,z,e]=linear_PCBC_iteration(x,y,W,z,a,eta,zeta,vartheta);
%update the response of a processing stage in the linear PC/BC model. This
%uses Harpur's linear mechanism of competition.
e=x-(W'*y);
y=(1-eta-vartheta).*y+zeta.*(W*e);
y=y+eta.*(a+z);

z=W'*y; %feedback to lower processing stage


