function [Y2,Y1]=linear_PCBC_attention_model(x,W2,attn,eta,zeta,vartheta,stim_off)
%set network parameters
iterations=20;
[n,m]=size(W2);
W1=eye(m);

%initialise activation values
y1=zeros(m,1);
y2=zeros(n,1);
z2=zeros(m,1);
Y2=y2;
Y1=y1;

%iterate to calculate node activations
for t=2:iterations
  %calculate response of 1st processing stage
  [y1]=linear_PCBC_iteration(x,y1,W1,z2,attn(:,:,1),eta,zeta,vartheta);

  %calculate response of 2nd processing stage
  [y2,z2,e2]=linear_PCBC_iteration(y1,y2,W2,0,attn(1:n,:,2),eta,zeta,vartheta);
  
  Y2=[Y2,y2]; %return time varying response of stage 2
  Y1=[Y1,y1]; %return time varying response of stage 1

  %remove input to network
  if t==stim_off, x=x.*0; end
end
