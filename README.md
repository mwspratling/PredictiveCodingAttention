----------------------------------------------------------------------------
# INTRODUCTION
----------------------------------------------------------------------------

This code implements the simulation results reported in:

[M. W. Spratling (2008) Predictive Coding as a Model of Biased Competition
in Visual Attention. Vision Research, 48(12):1391–1408.](https://nms.kcl.ac.uk/michael.spratling/Doc/visres08.pdf)

Please cite this paper if this code is used in, or to motivate, any publications. 

----------------------------------------------------------------------------
# USAGE
----------------------------------------------------------------------------

This code requires MATLAB. It was tested with MATLAB Version 7.7 (R2008b).

To use this software:
```
    run matlab 
    cd to the directory containing this code 
    run one of the functions with the name pc_*
```
Each of the pc_* functions runs one of the experiments reported in the above
publication. The following table gives details of which MATLAB function produces
each figure in this article.

Figure  |   MATLAB Function
--------|---------------------------------------------------------------
3		|	pc_spatial_selection.m
4		|	pc_spatial_selection_contrast.m
6		|	pc_featural_selection.m
7		|	pc_spatial_tuning_curve.m
8		|	pc_featural_tuning_curve.m
9		|	pc_posner.m
10		|	pc_binding.m
11		|	pc_binding_spatial.m
